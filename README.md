# AWS-UTILS

AWS-UTILS is a collection of shell scripts/functions/aliases that simplify interacting
with the AWS CLI. These tools can be increasingly useful in situations where you are
managing multiple AWS profiles and have EC2 hosts that are started/stopped and do not
have static elastic IP addresses.

Please see the wiki https://gitlab.com/jason.buckner/aws-utils/wikis/home for more details and
usage scenarios.

## Prerequisites

1. You will need an AWS account and some EC2 instances for utilizing these tools.

2. Use of these utilities pre-supposes that they will be located on a \*nix system (Ubuntu, OS X, ...) where
   the AWS CLI has been installed and configured. There are plenty of resources available for walking
   through that process (see https://docs.aws.amazon.com/cli/index.html)

3. The use of these tools assumes some working knowledge of ssh and scp from the command line

## Installation

The intent of these tools is to be simple and lightweight. Any one of the utilities
should be able to be copied into a directory on the users PATH and executed.

These tools may also be configured via environment variables to streamline usage
and simplify workflow.

## Usage

Below shows the options and basic usages of the tools. The tools provide several command
line options that can be simplified by using environment variables that can be sourced
from AWS profile-specific profile files.

---

### [e-ssh](https://gitlab.com/jason.buckner/aws-utils/wikis/Home/Scripts#e-ssh) - ssh to an EC2 instance using only the instance-id

##### Example

```
buck@buck ~/Documents/gitlab/aws-utils(develop *)$ e-ssh -i i-051ca1e7ec036f79e -p profile-A -k ~/.ssh/profile_A_key.pem
```

##### Output

```
✔ i-051ca1e7ec036f79e is a valid instance-id
✔ 'profile-A' is valid aws cli profile
✔ i-051ca1e7ec036f79e is running
✔ Connecting to EC2 host: 53.113.12.136
Welcome to Ubuntu 18.04.3 LTS (GNU/Linux 4.15.0-1054-aws x86_64)
.
.
.
Last login: Mon Dec  2 21:21:53 2019 from ...
ubuntu@ip-172-33-38-152:~$
```

##### Help

```
e-ssh -h
```

---

### [e-scp](https://gitlab.com/jason.buckner/aws-utils/wikis/Home/Scripts#e-scp) - scp a file to or from an EC2 host using the instance-id

##### Example

```
buck@buck ~/Documents/gitlab/aws-utils(develop *)$ e-scp -i i-051ca1e7ec036f79e \
-p profile-A \
-k ~/.ssh/profile_A_key.pem \
-s /tmp/from-local.txt \
-e /tmp/received-from-local.txt \
-d TO
```

##### Output

```
✔ i-051ca1e7ec036f79e is a valid instance-id
✔ 'siolta' is valid aws cli profile
✔ i-051ca1e7ec036f79e is running
✔ successfully copied from /tmp/from-local.txt to ubuntu@53.113.12.136:/tmp/received-from-local.txt
```

##### Help

```
e-scp -h
```

---

### [e-start-ssh](https://gitlab.com/jason.buckner/aws-utils/wikis/Home/Scripts#e-start-ssh) - attempt to start instance if not running prior to connecting by ssh

##### Example

```
$ e-start-ssh -i i-0151613a39846d9fd -p siolta  -k ~/.ssh/pipeline_key.pem
```

##### Output

```
✔ i-0151613a39846d9fd is a valid instance-id
✔ 'siolta' is valid aws cli profile
✔ i-0151613a39846d9fd is NOT running
✔ Starting instance i-0151613a39846d9fd
✔ i-0151613a39846d9fd has been started successfully
✔ i-0151613a39846d9fd is running
✔ checking for sshd daemon on 54.202.173.117
✔ sshd is running on 54.202.173.117
✔ Connecting to EC2 host: 54.202.173.117
Welcome to Ubuntu 18.04.3 LTS (GNU/Linux 4.15.0-1056-aws x86_64)
.
.
Last login: Fri Dec  6 18:33:53 2019 from ...
ubuntu@ip-172-31-36-179:~$
```

##### Help

```
e-start-ssh -h
```
